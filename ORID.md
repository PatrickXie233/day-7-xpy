**daodaoO (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview:在同学的引导下我成功完成了昨天的作业并全班展示，我知道自己还有许多stream的用法还不熟，在以后的学习中要多练习
2. concept map：我们绘制的是refactor的concept map，相比于上一周的concept map练习，我们小组很快进入状态，有条不紊的绘制着refactor，虽然最终结果有一些没考虑到的地方，但能感觉到我们小组默契度显著提高了
3. restful api：restful api是对url的一系列规则，用于更规范的命名以及定位资源，我们可以直接通过url与方法知道这个接口是干什么的。
4. pair programming：就如上课说的两个人抱在一起编程，有着聚焦任务，提高效率，知识传递等好处，在后续的spring boot实践中，我作为不太懂spring boot 的小白，很深刻的感受到了知识传递这一好处。
5. spring boot：今天spring boot的实践有许多问题，后续也都解决了并学会了许多springboot的开发方法，如注解，springboot的一些问题解决方法。

**R (Reflective): Please use one word to express your feelings about today's class.**

略显困难

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

今天最有意义的应该是下午的springboot实践，我和区良在两个地方卡了很久，但都最终解决了，并学会了许多东西

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

在学习新的开发方法时需要提前学习里面的知识，像今天卡在接口接收不到我们定义的类，后来老师提醒我们说是因为没有空参构造导致的，这让我更深刻的理解到实践得要现有知识作为基础。