package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompanyRepository {
    List<Company> companyList = new ArrayList<>();

    public CompanyRepository() {
    }

    public List<Company> getCompanyList() {
        return companyList;
    }

    public Company getCompanyById(int id) {
        return companyList.stream()
                .filter(company -> company.getId() == id)
                .findFirst().orElse(null);
    }

    public int createCompany(Company company) {
        company.setId(generateId());
        companyList.add(company);
        return company.getId();
    }

    public List<Company> getCompanyListByPage(int page, int size) {
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size).collect(Collectors.toList());
    }

    public Company updateCompany(int id, Company company) {
        Company findCompany = companyList.stream()
                .filter(company1 -> company1.getId() == id)
                .findFirst().orElse(null);
        if (findCompany == null) {
            return null;
        }
        findCompany.setName(company.getName());
        return findCompany;
    }

    public int deleteCompany(int id) {
        Company company = companyList.stream().filter(companyItem -> companyItem.getId() == id).findFirst().orElse(null);
        if (company == null) {
            return 0;
        }
        companyList.remove(company);
        return company.getId();
    }

    private Integer generateId() {
        int maxId = companyList.stream().mapToInt(Company::getId).max().orElse(0);
        return maxId + 1;
    }
}
